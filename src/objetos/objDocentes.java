/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package objetos;

import java.util.Date;

/**
 *
 * @author AAQ
 */
public class objDocentes extends objUsuarios{
    
    private String carrera;
    private String cantCursos;

    public objDocentes(String carrera, String cantCursos, String nombre, Date fecha, String genero, String tipo) {
        super(nombre, fecha, genero, tipo);
        this.carrera = carrera;
        this.cantCursos = cantCursos;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public String getCantCursos() {
        return cantCursos;
    }

    public void setCantCursos(String cantCursos) {
        this.cantCursos = cantCursos;
    }
    
    
    
}
