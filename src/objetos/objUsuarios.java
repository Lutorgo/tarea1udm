/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package objetos;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author AAQ
 */
public class objUsuarios {
    
    private String nombre;
    private Date fecha;
    private String genero;
    private String tipo;

    public objUsuarios(String nombre, Date fecha, String genero, String tipo) {
        this.nombre = nombre;
        this.fecha = fecha;
        this.genero = genero;
        this.tipo = tipo;
    }

    public static ArrayList listaUsuarios = new ArrayList<>();
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    
}
