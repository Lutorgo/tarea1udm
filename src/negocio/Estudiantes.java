/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package negocio;

import datos.BDUsuarios;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import objetos.*;

/**
 *
 * @author AAQ
 */
public class Estudiantes {

    BDUsuarios usuarios = new BDUsuarios();

    public String[] devuelveDatosEstudiante() {
        String telefono = "435345";
        String direccion = "San Carlos";

        String[] datos = new String[2];
        datos[0] = telefono;
        datos[1] = direccion;

        return datos;
    }

    public void insertarEstudiantes(ArrayList<objEstudiantes> listaestudiantes) {
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String datos = "";
        for (int i = 0; i < listaestudiantes.size(); i++) {
            String nombre = listaestudiantes.get(i).getNombre();
            String fecha = formato.format(listaestudiantes.get(i).getFecha());
            String genero = listaestudiantes.get(i).getGenero();
            String tipo = listaestudiantes.get(i).getTipo();
            String telefono = listaestudiantes.get(i).getTelefono();
            String direccion = listaestudiantes.get(i).getDireccion();
            datos = nombre + "," + fecha + "," + genero + "," + tipo + "," + telefono + "," + direccion;
            usuarios.insertarEnArchivo(datos);
        }
    }

}
