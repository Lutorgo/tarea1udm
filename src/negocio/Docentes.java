/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package negocio;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import objetos.*;
import datos.BDUsuarios;

/**
 *
 * @author AAQ
 */
public class Docentes {

    BDUsuarios usuarios = new BDUsuarios();

    public String[] devuelveDatosDocente() {
        String carrera = "ISW";
        String canCursos = "3";

        String[] datos = new String[2];
        datos[0] = carrera;
        datos[1] = canCursos;

        return datos;
    }

    public void insertarDocentes(ArrayList<objDocentes> listadocentes) {
        DateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String datos = "";
        for (int i = 0; i < listadocentes.size(); i++) {
            String nombre = listadocentes.get(i).getNombre();
            String fecha = formato.format(listadocentes.get(i).getFecha());
            String genero = listadocentes.get(i).getGenero();
            String tipo = listadocentes.get(i).getTipo();
            String carrera = listadocentes.get(i).getCarrera();
            String cantCursos = listadocentes.get(i).getCantCursos();
            datos = nombre + "," + fecha + "," + genero + "," + tipo + "," + carrera + "," + cantCursos;
            usuarios.insertarEnArchivo(datos);
        }
    }
}
