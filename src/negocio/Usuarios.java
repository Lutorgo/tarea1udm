/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package negocio;

import java.util.ArrayList;
import datos.*;

/**
 *
 * @author AAQ
 */
public class Usuarios {

    BDUsuarios usuarios = new BDUsuarios();

    public ArrayList<String> LeerUsuarios() {
        ArrayList<String> lista = usuarios.LeerDesdeArchivo();
        return lista;
    }

    public ArrayList<String> ModificarUsuarios() {
        ArrayList<String> lista = usuarios.ModificarDatosArchivo();
        return lista;
    }

    public ArrayList<String> EliminarUsuarios() {
        ArrayList<String> lista = usuarios.EliminarDatosArchivo();
        return lista;
    }
    
    public ArrayList<String> ActualizarUsuarios() {
        ArrayList<String> lista = usuarios.ActualizarDatosArchivo();
        return lista;
    }
}
