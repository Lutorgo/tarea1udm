/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package datos;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import presentacion.FrmUDM;

/**
 *
 * @author AAQ
 */
public class BDUsuarios {

    public void insertarEnArchivo(String datosUsuario) {
        try {
            File archivo = new File("ListaUsuarios.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo, true));
            archi.write(datosUsuario + "\r\n");
            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al escribir en el archivo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    public ArrayList<String> LeerDesdeArchivo() {
        ArrayList<String> lista = new ArrayList<>();
        try {
            File archivo = new File("ListaUsuarios.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                lista.add(archi.readLine());
            }
            archi.close();

            for (int i = 0; i < 1; i++) {

                for (String lista2 : lista) {

                    String[] parte = lista2.split(",");
                    String parte1 = parte[0];
                    String parte2 = parte[1];
                    String parte3 = parte[2];
                    String parte4 = parte[3];
                    String parte5 = parte[4];
                    String parte6 = parte[5];

                    DefaultTableModel modelo = (DefaultTableModel) FrmUDM.tabla1.getModel();

                    Object[] fila = new Object[6];

                    //Sección 3
                    fila[0] = parte1;
                    fila[1] = parte2;
                    fila[2] = parte3;
                    fila[3] = parte4;
                    fila[4] = parte5;
                    fila[5] = parte6;

                    //Sección 4
                    System.out.println(parte1 + " " + parte2 + " " + parte3 + " " + parte4 + " " + parte5 + " " + parte6);
                    modelo.addRow(fila);

                    //Sección 5
                    FrmUDM.tabla1.setModel(modelo);
                }

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer en el archivo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return lista;
    }

    public ArrayList<String> ActualizarDatosArchivo() {
        ArrayList<String> lista = new ArrayList<>();
        try {
            File archivo = new File("ListaUsuarios.txt");
            BufferedReader archi = new BufferedReader(new FileReader(archivo));
            while (archi.ready()) {
                lista.add(archi.readLine());
            }
            archi.close();
            DefaultTableModel modelo = (DefaultTableModel) FrmUDM.tabla1.getModel();
            for (int i = 0; i < 1; i++) {

                for (String lista2 : lista) {

                    String[] parte = lista2.split(",");
                    String parte1 = parte[0];
                    String parte2 = parte[1];
                    String parte3 = parte[2];
                    String parte4 = parte[3];
                    String parte5 = parte[4];
                    String parte6 = parte[5];

                    Object[] fila = new Object[6];

                    //Sección 3
                    fila[0] = parte1;
                    fila[1] = parte2;
                    fila[2] = parte3;
                    fila[3] = parte4;
                    fila[4] = parte5;
                    fila[5] = parte6;

                    //Sección 4
                    System.out.println(parte1 + " " + parte2 + " " + parte3 + " " + parte4 + " " + parte5 + " " + parte6);
                    modelo.addRow(fila);

                    //Sección 5
                    FrmUDM.tabla1.setModel(modelo);
                    for (int j = 0; j < lista.indexOf(j); j++) {
                        modelo.removeRow(i);
                    }
                    modelo.fireTableDataChanged();
                }

            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer en el archivo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }
        return lista;
    }

    public ArrayList<String> ModificarDatosArchivo() {
         ArrayList<String> lista = new ArrayList<>();

        DefaultTableModel modelo = (DefaultTableModel) FrmUDM.tabla1.getModel();
        int filaSeleccionada = FrmUDM.tabla1.getSelectedRow();

        if (filaSeleccionada != -1) {

            modelo.removeRow(filaSeleccionada);
        }
        try {
            File archivo = new File("ListaUsuarios.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo));
            Object edit = new Object [6];
            for (int i = 0; i < modelo.getRowCount(); i++) {
                for (int j = 0; j < modelo.getColumnCount(); j++) {
                    archi.write((String) (modelo.getValueAt(i, j)));
                     edit = modelo.getValueAt(i, j);
                    
                    
                    
                    
                    if (j < modelo.getColumnCount() - 1) {
                        archi.write(",");
                    }
                }
                System.out.println(edit.toString()+"  OBJETO A STRING");
                archi.newLine();
            }

            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer en el archivo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }

        return lista;
    }

    public ArrayList<String> EliminarDatosArchivo() {
        ArrayList<String> lista = new ArrayList<>();

        DefaultTableModel modelo = (DefaultTableModel) FrmUDM.tabla1.getModel();
        int filaSeleccionada = FrmUDM.tabla1.getSelectedRow();

        if (filaSeleccionada != -1) {

            modelo.removeRow(filaSeleccionada);
        }
        try {
            File archivo = new File("ListaUsuarios.txt");
            BufferedWriter archi = new BufferedWriter(new FileWriter(archivo));
       
            for (int i = 0; i < modelo.getRowCount(); i++) {
                for (int j = 0; j < modelo.getColumnCount(); j++) {
                    archi.write((String) (modelo.getValueAt(i, j)));
                    if (j < modelo.getColumnCount() - 1) {
                        archi.write(",");
                    }
                }
                archi.newLine();
            }

            archi.close();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al leer en el archivo", "Mensaje de Error", JOptionPane.ERROR_MESSAGE);
        }

        return lista;
    }
}
